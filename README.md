Auto-Kiosk

A fully automated web browser kiosk solution running on KDE Plasma using Brave Browser.  This was built on Garuda Linux and iMac18,1/iMac16,1.  Everything is meant to be manually installed and configured.  One can build a package or image to automate deployment of this.  Assumes the user account is named "human". (this is how I configure my images)

The following features are optional, place into the "Features-Enabled" folder to enable, "Features-Disabled" to disable then restart:
Auto-Kiosk: Enables loading the web browser.  Consider this a master "On/Off" switch.
Auto-Reload: Automatically reload the browser if all windows are closed or idle timeout is reached.
Display-Message: Display an instructional message.  When clicking "OK" the URL(s) will be loaded.  This has the added benefit of a fresh page load when a user begins to use the kiosk.
Minimize-Windows: All windows will be minimized before displaying the message or loading the browser.
Presentation-Mode: Browser will be loaded into full screen mode instead of a standard window and mouse cursor will be hidden.
Disable-Configuration-Persistence: All paths specified in ~/Auto Kiosk/.Auto-Kiosk/Configuration-Templates/ will be re-applied on shutdown  (deleted then copied from template).  This makes the user account tamper-proof.
Disable-Document-Persistence: ALL FILES will be deleted from the user's home on shutdown!  Highly recommended to use in conjunction with "Disable-Configuration-Persistence".  I setup hourly @HOME BTRFS snapshots on Garuda.  USE THIS WITH CAUTION AND HAVE EXTERNAL BACKUPS OF IMPORTANT DATA!
IdleTimeout.txt: Set the automatic reload on idle timeout, in minutes.
URLs.txt: Specify one or more URLs to be loaded in the browser.  Each one is loaded as a tab.
Update-Configuration-Templates.bash: Make your configuration changes for the user, specify paths you want to maintain from a template in Configuration-Templates (you can copy the files/folders into this folder) then run this script as root to update your configuration template from the current user.  THIS MUST BE DONE AT LEAST ONCE so the permissions protect this folder from user tampering.

The following features are always enabled:
Daily system sleep at 6PM to 6AM.
Daily restart at 6AM.
Monthly updates on the first of the month at 00:00 with restart at 03:00 to allow additional installs after booting a new kernel.

Common use cases:
A "book search station" in a library.
A "book checkout station" in a library.
Site wide daily announcements host.
Generic use computer lab. (mostly web browsing uses)

Dependancies:
Garuda Linux
plasma-desktop
brave
zenity
xprintidle
xdotool
unclutter

Pre-requisites:
X11.
Set "Auto-Kiosk.bash" as a login script.
Set Plasma to auto login as user and log back in if logged out.
Window rule for Brave to apply initially "Maximized Horizontally" and apply initially "Maximized Vertically".
Window rules for Zenity to force "Keep above other windows" and force "No titlebar and frame".
Disable Garuda Maintenance prompts.
Disable any other automatic software update. (we are implementing our own)
Set computer to never automatically shutdown.
Enabling systemd units.  If a systemd .timer then only enable the .timer, if just a .service then just enable the .service.

Make your life easier:
Add ~/.Auto-Kiosk to the Dolphin sidebar.

Clonezilla image pre-build checklist:
Restart
Update "Image Date" in "Image Notes.txt"
update
Check Discover updates
Clear package cache and caches with Garuda Assistant app
pacman -Scc
Confirm we are using X11
Empty trash
Remove recently opened from Kate
Clear clipboard history
Clear notification history
Remove all wireless and wired connections
Update-Configuration-Templates.bash WITHOUT BRAVE RUNNING!
Restart
Remove root and home snapshots
Create snapshot of home: Image Build
Create snapshot of root: Image Build
update-grub

Thanks to Daniel Andersson for the idle script:
https://superuser.com/questions/638357/execute-a-command-if-linux-is-idle-for-5-minutes

davidjo for the audio driver:
https://github.com/davidjo/snd_hda_macbookpro

And thanks to Applebit924 for the help!

License:
There is no software license for this as that is a false concept, at best a suggestion.  You have free will and can make any decision you want to do, or not do, with the arrangement of bits on your hardware.  Welcome to reality.
