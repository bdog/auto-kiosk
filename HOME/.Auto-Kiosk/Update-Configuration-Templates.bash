#!/bin/bash
while read; do
    if [ -e /home/human/"$REPLY" ]; then
        mkdir -p /home/human/.Auto-Kiosk/Configuration-Templates-New
        cp -rf /home/human/"$REPLY" /home/human/.Auto-Kiosk/Configuration-Templates-New/
    fi
done< <(ls -1A /home/human/.Auto-Kiosk/Configuration-Templates)

rm -rf /home/human/.Auto-Kiosk/Configuration-Templates
mv -f /home/human/.Auto-Kiosk/Configuration-Templates-New /home/human/.Auto-Kiosk/Configuration-Templates
# Prevent "Profile is locked" warnings in Brave.
rm -f /home/human/.Auto-Kiosk/Configuration-Templates/.config/BraveSoftware/Brave-Browser/Singleton*
chown -R root /home/human/.Auto-Kiosk
chmod -R 755 /home/human/.Auto-Kiosk
