#!/bin/bash

# Prevent "Profile is locked" warnings in Brave.
rm -f ~/.config/BraveSoftware/Brave-Browser/Singleton*

URLsToLoad=$(cut -f 1 ~/.Auto-Kiosk/URLs.txt | grep -v "^#")
Timeout=$(cut -f 1 ~/.Auto-Kiosk/IdleTimeout.txt | grep -v "^#")

# Header for the instructional message.
MessageHeader='<span foreground="red" font="32">Automatic Kiosk\n\n</span>'

# Warning that we will be deleting ALL user files on shutdown!
if [ -e ~/.Auto-Kiosk/Features-Enabled/Disable-Document-Persistence ]; then
    DisableDocumentPersistenceWarning='<span foreground="red" font="32">ALL FILES\nARE DELETED\nON SHUTDOWN!\n\n</span>'
fi

# The main body of the instructional message
InfoMessage='<span foreground="lime" font="28">Close browser windows when finished to logout of all web accounts.</span><span foreground="yellow" font="18">\n\nIdle Reset: '$Timeout' Minutes</span>'

# Trigger timeout in milliseconds, do not modify!
IDLE_TIME=$(($Timeout*60*1000))

MinimizeAllWindows() {
if [ -e ~/.Auto-Kiosk/Features-Enabled/Minimize-Windows ]; then # All windows will be minimized before loading message or browser.
    active_window_id=$(xdotool getactivewindow)
    for window_id in $(xdotool search --onlyvisible ".*"); do
        xdotool windowminimize $window_id
    done
fi
}

ZenityMessage() {
    pkill brave --signal 9
    pkill zenity --signal 9
    zenity --info --text "$MessageHeader""$DisableDocumentPersistenceWarning""$InfoMessage"
}

StandardBraveLaunch() {
    pkill brave --signal 9
    MinimizeAllWindows
    brave --app-auto-launched --incognito $URLsToLoad &
}

WaitForAppToOpen() {
    while [[ "$IsBraveOrZenityOpen" == "" ]]; do # Wait for the app to open.
        sleep 1
        IsBraveOrZenityOpen=$(pgrep 'brave|zenity')
    done
}

# This is the AutoKiosk subroutine.
AutoKiosk() {
    if [ -e ~/.Auto-Kiosk/Features-Enabled/Auto-Reload ]; then # Kiosk is allowed to automatically reload.
            echo "Killing and ReOpening Zenity/Brave @ $(date)"
            if [ ! -e ~/.Auto-Kiosk/Features-Enabled/Presentation-Mode ]; then # We are not using Presentation-Mode.
                if [ -e ~/.Auto-Kiosk/Features-Enabled/Display-Message ]; then # Instructional message is enabled.
                    ZenityMessage; rr=$?
                fi
                if [[ $rr == "0" ]]; then # User clicked "OK" on the instructional message, proceed to load browser.
                    StandardBraveLaunch
                elif [[ $rr != "" ]]; then # Don't launch browser, we probably just killed the zenity message.
                    echo "Do nothing"
                else
                    StandardBraveLaunch
                fi
            else # We are using Presentation-Mode.
                pkill brave --signal 9
                unclutter &
                brave --incognito --start-fullscreen $URLsToLoad &
            fi
    fi
}

LoopLoader() {
    if [[ $rr != "0" ]]; then
        MinimizeAllWindows
        AutoKiosk
    fi
}



# Preparing variables for the loop
sleep_time=$IDLE_TIME
triggered=false

# This is the main loop to allow automatic reloading
MainLoop(){
if [ -e ~/.Auto-Kiosk/Features-Enabled/Auto-Kiosk ]; then # Shared use computer: Enable idle reload, use incognito window and display instructional message.
    while [ 1 ]; do
        sleep 1
        IsBraveOrZenityOpen=$(pgrep 'brave|zenity')
        SDATE=$(date +%s)
        echo "Start - $SDATE"
        if [[ ! "$IsBraveOrZenityOpen" ]]; then # They are not open.
            AutoKioskProcesses=$(pgrep Auto-Kiosk -c)
            if [[ $AutoKioskProcesses == "1" ]]; then # Do NOT do this if a background subroutine is running.
                LoopLoader &
                WaitForAppToOpen
            fi
        else
            idle=$(xprintidle)
            if [ $idle -ge $IDLE_TIME ]; then # We have gone idle.
                if ! $triggered; then
                    LoopLoader &
                    WaitForAppToOpen
                    triggered=true
                    sleep_time=$IDLE_TIME
                fi
            else
                triggered=false
                sleep_time=$((IDLE_TIME-idle+100)) # Give 100 ms buffer to avoid frantic loops shortly before triggers.
            fi
        fi
        FDATE=$(date +%s)
        echo "Finish- $FDATE"
    done
else # Not an Automatic Kiosk station, just open Brave once and then we're done.
    brave &
fi
}



MainLoop
